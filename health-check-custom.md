# Health Check custom SOW

## Delivery

1. [ ] Hold discovery call with the customer to establish about their goals and motivations for engaging in this service.
1. [ ] Make sure the customer can provide access to the appropriate monitoring systems needed. This can include gitlab's Prometheus and Grafana or can be a 3rd party monitoring tool (e.g. New Relic, App Dynamics) or set of tools (Cloud infrastructure monitoring + APM tooling). **If there is no monitoring in place, or access cannot be granted, do not proceed with any load testing**. We can help them [install/configure prometheus and grafana](configure-prometheus.md), but it will require a change request if it was not already included in the engagement SOW. 

### Determine type of health check
1. [ ] We will be selling readiness assessments with 2 kinds of load put on the system to measure the system characteristics - GPT or production user load.

Determine the type of system load that was scoped by inspecting the SOW activities.

#### GPT
1. [ ] If it's GPT based, make sure to ask the customer for the system URL and frontend/backend access to this system.
1. [ ] Familiarize yourself with GPT by checking out [the GPT quick start](https://about.gitlab.com/handbook/support/workflows/gpt_quick_start.html).
1. [ ] [Clone GPT](https://gitlab.com/gitlab-org/quality/performance) on a server that has API access to the production-like system.
1. [ ] Discuss how much load to put on the system by asking the customer how many users they're planning to accomodate with their GitLab Self Managed deployment.
1. [ ] Run GPT with the given settings. Use this [step by step guide](/using-gpt.md) on how to use GPT for load testing. 

#### User Production load
1. [ ] Get access to the monitoring systems that the Customer has in place. Make sure you have access to:
    - Component (Rails, Praefect, PostgreSQL, Gitaly) CPU, Memory, Disk utilization, Disk I/O
    - Load balancer http response codes and response latency
    - Availability (uptime) data for all components
    - Number of jobs processed per unit time and average job queue for SideKiq and
1. [ ] Establish a monitoring period with typical production workload (e.g. don't do it during week with a holiday). Typically this should be at least 7 days. The Customer might have this data captured already.
1. [ ] After the monitoring period completes, gather data similar to the graphs in this [example report](https://docs.google.com/document/d/1_1Jy24KMfsFDJzrXpBS0b5jXZfjbiPTiPSa6LshRYrs/edit#heading=h.z1jeb7kk42ne).

### Evaluate the results
1. [ ] Evaluate the GPT/monitoring results. If there are failing events (GPT) or suspicious monitoring results (user load), investigate the deployment architecture to determine bottlenecks; typicallly component CPU, memory, or disk I/O, network throughput, API rate limiting, 3rd party integration issues.
1. [ ] Consider using [fast-stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats) to investigate resource usage across the endpoints and. They should help you find out which components are constraining the performance of a given endpoint on a specific node.
1. [ ] Consider using [fio](https://docs.gitlab.com/ee/administration/operations/filesystem_benchmarking.html) to test I/O performance if you notice interacting with Git data is slower than expected.
1. [ ] You can ask for help in the [#self-managed-environment-triage](https://gitlab.slack.com/archives/C015V8PDUSW/p1611682200009800) Slack channel if you get stuck on interpreting the results.
1. [ ] Capture learnings and recommendations in a readiness report (**DO NOT FIX ANYTHING FOR THEM UNLESS NOTED IN THE SOW**).
