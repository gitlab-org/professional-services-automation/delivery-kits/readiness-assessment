# Health Check SKU

The Health Check SKU is a scaled down version of our Health Check service that specifically focuses on running GPT and evaluating the results of the test

*There is no user production load monitoring included in this service*

## Delivery

1. [ ] Hold discovery call with the customer to establish and discuss their goals and motivations for engaging in this service.
    1. [ ] Discuss how much load to put on the system by asking the customer how many users they're planning to accomodate with their GitLab Self Managed deployment.
1. [ ] Schedule a day and time to run GPT against the customer's GitLab instance.
    - If this is running against a production instance, schedule the test outside of business hours and and after any critical customer deadlines or due dates.
    - If the customer uses a non-production GitLab instance with an identical architecture to their production instance, run GPT against that instance instead of a production instance
1. [ ] Prepare the server used to execute GPT by walking through the `Running GPT` steps below
1. [ ] Run GPT on the agreed upon day at the agreed upon time. Keep the customer in the loop while this test is running and ideally run through the test while on a call with a GitLab admin on the customer side
1. [ ] Evaluate the results (see below)
1. [ ] Write up a report covering:
    - Existing architecture based on the architecture review
    - Results of the GPT test
    - Any suggested improvements to their instance

### Running GPT
1. [ ] Familiarize yourself with GPT by checking out [the GPT quick start](https://about.gitlab.com/handbook/support/workflows/gpt_quick_start.html).
1. [ ] [Clone GPT](https://gitlab.com/gitlab-org/quality/performance) on a server that has API access to the production-like system.
1. [ ] Discuss how much load to put on the system by asking the customer how many users they're planning to accomodate with their GitLab Self Managed deployment.
1. [ ] Run GPT with the given settings. Use this [step by step guide](/using-gpt.md) on how to use GPT for load testing. 

### Evaluate the results
1. [ ] Evaluate the GPT results. If there are failing events (GPT), investigate the deployment architecture to determine bottlenecks; typicallly component CPU, memory, or disk I/O, network throughput, API rate limiting, 3rd party integration issues.
1. [ ] Consider using [fast-stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats) to investigate resource usage across the endpoints. It should help you find out which components are constraining the performance of a given endpoint on a specific node.
1. [ ] Consider using [gitlabsos](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) and [SOSParser](https://gitlab.com/gitlab-com/support/toolbox/sosparser) to gather additional system data after running GPT
1. [ ] Consider using [fio](https://docs.gitlab.com/ee/administration/operations/filesystem_benchmarking.html) to test I/O performance if you notice interacting with Git data is slower than expected.
1. [ ] You can ask for help in the [#reference-architectures](https://gitlab.slack.com/archives/C015V8PDUSW) (formerly #self-managed-environment-triage) Slack channel if you get stuck on interpreting the results.
1. [ ] Capture learnings and recommendations in a readiness report (**DO NOT FIX ANYTHING FOR THEM UNLESS NOTED IN THE SOW**).