# Executive Summary

This report presents the findings of a thorough GitLab environment health check for Customer XYZ. We focus on performance and operational efficiency concerns, based on detailed information gathered during interactive customer sessions.
Customer XYZ has been facing intermittent GitLab issues across various components, impacting system reliability, productivity, and workflow continuity. Our main goal was to uncover the root causes of these ongoing problems.
We conducted an in-depth analysis of the current GitLab setup to pinpoint areas needing improvement. This report outlines our discoveries, insights, and concrete recommendations to enhance system performance and operational effectiveness.
Our aim is to resolve the identified issues and optimize Customer XYZ’s' GitLab environment, ensuring they can fully utilize GitLab's capabilities without the hindrances they've been experiencing.

## Environment Review

* Current State Architecture Diagram (insert the current Gitlab CNH High level architecture diagram here)

## General Architecture Information

* Version: Self-managed GitLab Enterprise Edition 16.x.y
* License: Premium
* Runner version: 16.x.y

## Deployment

* Platform: Amazon EKS (Elastic Kubernetes Service)
* Configuration: Single regional cluster
* Deployment method: GitLab Helm chart
* Isolation: Namespace-based, with select components on dedicated node groups

## Data Services

* Storage: Amazon S3 for artifacts, cache, and backups
* Databases: PostgreSQL and Redis (external to EKS)
* Gitaly: Currently runs as a StatefulSet within EKS

## Kubernetes-managed components

The core GitLab infrastructure, managed by Kubernetes, encompasses a range of components that are essential for GitLab's operation.
Core components:

* HA-Proxy Controller - Handles incoming HTTP/S requests and routes them to the appropriate services.
* GitLab Shell - Enables SSH access for Git repository operations.
* GitLab Pages - Hosts static websites directly from a GitLab repository.
* GitLab Exporter - Collects and exports GitLab performance metrics.
* GitLab Toolbox - Provides various utilities for GitLab maintenance tasks.
* GitLab Workhorse - Acts as a smart reverse proxy for GitLab, handling "long" HTTP requests.
* GitLab Web Service (Puma) - Serves GitLab's web interface and API requests.
* Gitaly - Handles Git repository storage, specifically pinned to a designated node for performance reasons.
* Sidekiq - Processes background jobs, such as emails and repository updates.

## GitLab Project/User Overview

* Insert the dash board screenshot from the exsiting deployment here.

## GitLab Configuration

This chapter discusses the current GitLab configuration and improvement considerations.

# Current GitLab Configuration

The current GitLab values.yaml that was provided by the customer was used for the analysis. In this chapter, we will outline spotted recommendations.
To enhance the configurations and recommendations for your GitLab deployment within a Kubernetes cluster, please consider the following structured advice:

* Migrate Gitaly to a standalone node

Rationale: Hosting Gitaly on a standalone node is a best practice due to performance and reliability considerations. This setup is required over deploying Gitaly within a Kubernetes cluster, which is unsupported by Gitlab at the moment, although there is some progress recently made to make Gitaly compatible to run on Kubernetes, however from a support perspective, it is still not an [officially supported deployment.](https://docs.gitlab.com/ee/administration/gitaly/kubernetes.html)

* Action: Plan and execute the migration of Gitaly to a dedicated standalone node.

## Current deployment

GitLab is deployed on AWS single region leveraging AWS EKS, RDS, S3, and Elasticache. After an environment was created the GitLab helm chart  was employed to deploy GitLab to AWS Elastic Kubernetes Service  (EKS) using the official Helm chart.

## GitLab Performance Tool (GPT)

The GitLab Performance Toolkit was run on 2024-07-17 against the GitLab Production instance, using a 2K reference target, as per our guidance in the GPT documentation. All tests passed except a few as mentioned below . The final score was 95% - This value is calculated from all of the test results and presented as a distilled value to show how well the environment performed overall. Typically a well performing environment should be above 90%.

* Comparing Results: We post our [own results](https://gitlab.com/gitlab-org/quality/performance/wikis/home) for transparency as well as allowing users to compare. Currently, you’ll find the following results on our Wiki:

* [Latest Results:](https://gitlab.com/gitlab-org/quality/performance/wikis/Benchmarks/Latest) Our automated CI pipelines run multiple times each week and will post their result summaries to the wiki here each time.

* [GitLab Versions:](https://gitlab.com/gitlab-org/quality/performance/wikis/Benchmarks/GitLab-Versions) A collection of performance test results done against several select release versions of GitLab.

## Production Instance GPT Results

* Environment: 2K
* Environment Version: 16.6.6-ee
* Option: 60s_40rps
* Date: 2024-07-17
* Run Time: 1h 40m 36.84s (Start: 20:23:04 UTC, End: 22:03:40 UTC)
* GPT Version: v2.14.0

### Overall Results Score: 95%

## Procedure

The GitLab Performance Tool (GPT) can be used to:

* Create test data on a GitLab instance.
* Measure performance of a GitLab instance.
* Compare the performance metrics to the public reference architecture performance test results.

## Summary

The overall test result is `95%`, which is a “Pass” as per GitLab instance testing parameters. However we want to elaborate on the API’s that failed the GPT test and possible reasons for the failure.

## List of failed projects

```shell
api_v4_groups_merge_requests  | 40/s | 0.44/s (>3.20/s)   | 50055.82ms | 50065.11ms (<17500ms) | 0.00% (>99%)   | FAILED¹² 


api_v4_projects_merge_requests | 40/s | 38.8/s (>32.00/s)  | 214.16ms   | 287.31ms (<200ms)     | 100.00% (>99%) | FAILED²  


api_v4_projects_project | 40/s | 39.2/s (>32.00/s)  | 205.15ms   | 297.17ms (<200ms)     | 100.00% (>99%) | FAILED² 


api_v4_projects_project_pipelines  | 40/s | 39.29/s (>32.00/s) | 167.99ms   | 297.68ms (<200ms)     | 100.00% (>99%) | FAILED² 


git_clone                          | 2/s  | 0.09/s (>0.05/s)   | 233.14ms   | 462.77ms (<800ms)     | 50.00% (>99%)  | FAILED¹²


web_project_file_rendered    | 4/s  | 1.35/s (>2.56/s)   | 458.33ms   | 575.01ms (<1700ms)    | 100.00% (>99%) | FAILED¹²


web_project_file_source      | 4/s  | 0.23/s (>1.92/s)   | 679.32ms   | 1338.26ms (<2700ms)   | 100.00% (>99%) | FAILED¹²

```

Looking at the results, it appears that all failed results came with a side note (either 1, 2 or both).
This is how the sidenotes are described:

¹ Result covers endpoint(s) that have known issue(s). Threshold(s) have been adjusted to compensate.
² Failure may not be clear from summary alone. Refer to the individual test's full output for further debugging.

### Review

1) `api_v4_groups_merge_requests`. this API List groups merge requests (GET /groups/:id/merge_requests) . 
This endpoint usually loads Rails, Postgres, Gitaly components, could be helpful to run the test and monitor the metrics to check further.
significant failure over the threshold. could be helpful to run the test and monitor the metrics to check further.

2) `api_v4_projects_merge_requests_merge_request`, `api_v4_projects_project`, `web_project_file_rendered`, `web_project_file_source`, `api_v4_projects_project_pipelines` failures are slightly over than threshold, they can be omitted. it's recommended to run the test again on the new scaled up architecture to make sure the test is passing as expected.

3) git_clone - 50% of requests failed, this API Clone via HTTPS to clone from the specified branch (GET /:group/:project.git/info/refs?service=git-upload-pack)
looking into the .log file we can see that the "Request Failed" error=" request timeout" - seems like the clone failed with the timeout. The endpoint mainly loads Gitaly.

### Several Contributing Factors

* Gitaly Overload: The Gitaly service might be experiencing an excessive number of requests, overwhelming its capacity to process them efficiently.
* Request Timeouts: Due to the high load, Gitaly may fail to respond within the expected timeframe, resulting in request timeouts.
* Pod Instability: The Gitaly pod could become unresponsive under stress, potentially leading to automatic restarts by the orchestration system.
* Resource Unavailability: In some instances, the Gitaly service might be temporarily unavailable to handle incoming requests, possibly due to ongoing restarts or resource constraints.
These issues collectively can lead to degraded performance and intermittent failures in Git-related operations within the GitLab environment."

## KUBESOS Test Results

To understand the current load on the GitLab environment, we accessed:

* API requests
* Web requests
* Git fetches/pushes

These request types correspond to specific log files:

* API requests are recorded in api_json.log
* Web requests are logged in production_json.log
* Both Git fetches and pushes are documented in gitlaly.log

To calculate the RPS (request per second)  values for each category, we analyzed the log files from busy activity periods using the fast-stats to get an idea on the total RPS load on the system.

We have used KubeSOS to extract all configurations from the GItLab environment. Before we can get visual and organized information from those logs (using SOSParser), we need to convert the KubeSOS logs to the GitlabSOS format.

### Convert KubeSOS logs to GitlabSOS logs

* Create the folder:  mkdir -p var/log/gitlab
* Underneath that folder: cd var/log/gitlab && mkdir gitlab-workhorse sidekiq gitaly gitlab-rails
* Remove the time from the logs to make it parseable by faststats or SOSparser:

```shell
 cp gitaly_gitaly.log var/log/gitlab/gitaly/current
 sed -i '' 's/^.\{31\}//g' var/log/gitlab/gitaly/current
```

* Do this for all logs available

```shell
 sed -i '' 's/^.\{31\}//g' *.log
```

* For web service and production_json, use jq in order to split web service logs to sosparse parsable api_json and production_json:

```shell
 jq -c 'select(.subcomponent == "api_json")' < webservice_webservice.log > api_json.log
 jq -c 'select(.subcomponent == "production_json")' < webservice_webservice.log > production_json.log
```

* You can now also use faststats, download it this way:
  
```shell
 curl https://gitlab.com/gitlab-com/support/toolbox/fast-stats/uploads/ba5b5082452e51274d30d3dd66ab5465/fast-stats_0-8-3_macos_arm64.tar.gz -O /usr/local/bin

 cd /usr/local/bin

 tar -xf fast-stats_0-8-3_macos_arm64.tar.gz

 rm fast-stats_0-8-3_macos_arm64.tar.gz
```

* And use it by invoking  “fast-stats path-to-log-file”

## Gitaly Logs

### Totals

![alt text](<./img/Screenshot 2024-09-05 at 9.35.43 AM.png>)

## Top 10 projects by duration

![alt text](<./img/Screenshot 2024-09-05 at 10.07.34 AM.png>)

## Top 10 user  by duration

![alt text](<./img/Screenshot 2024-09-05 at 10.08.20 AM-1.png>)

## Top Client  by duration

![alt text](<./img/Screenshot 2024-09-05 at 10.09.22 AM.png>)

## Production Logs

### Totals

![alt text](<./img/Screenshot 2024-09-05 at 10.10.05 AM-1.png>)

## Top 5 Path by duration

![alt text](<./img/Screenshot 2024-09-05 at 10.11.04 AM.png>)

## Top 5 User by duration

![alt text](<./img/Screenshot 2024-09-05 at 10.11.35 AM.png>)

## API Logs

### Totals

![alt text](<./img/Screenshot 2024-09-05 at 10.12.09 AM.png>)

**Similarly, insert the tables for Top 5 path by duration, Top 5 Project by duraiton, and Top 5 users by duration for the API logs.**

## Overall Status

### Analysis of the system performance graphs

#### Insert the Production Log, Gitaly, API, and Sidekiq graphs for fast-stats output here

### Findings

* Above graphs are shown during the time kubeSOS (GitlabSOS) was taken, the load on Web is significantly higher than API endpoint, however nothing critical.

* The setup is rather risky for failure cases, this is related to the Gitaly setup, which was highlighted before

## Immediate Concern

* Customer XYZ's primary issue, prompting their engagement with Professional Services, stems from an unsupported GitLab deployment. The root cause appears to be that GitLab is currently running in a container, which is not a configuration that GitLab officially supports.
* Performance testing has revealed that the git_clone API, which is handled by Gitaly (GitLab's Git RPC service), is failing. This failure likely indicates that the existing Gitaly configuration is suboptimal for Customer XYZ's needs.
* To address this immediate concern, our recommendation is for Customer XYZ to transition to an external Gitaly service. This approach offers several advantages:
* Improved performance: By separating Gitaly from the main GitLab container, resource allocation can be optimized for Git operations.
  * Better scalability: External Gitaly services can be scaled independently, allowing for more efficient handling of repository-related tasks.
  * Enhanced stability: Isolating Gitaly reduces the risk of Git operations impacting other GitLab services and vice versa.
  * Easier maintenance: Updates and maintenance can be performed on Gitaly without affecting the entire GitLab instance.
  * Compliance with supported configurations: Moving to an external Gitaly aligns with GitLab's recommended deployment strategies, ensuring better support and compatibility.
* The process for implementing this solution is detailed in GitLab's documentation. This guide provides step-by-step instructions for setting up and configuring an external Gitaly service, including necessary changes to the GitLab Helm chart values.

### Recommendation

* As an immediate measure, Customer XYZ should move to an external gitaly.

## Performance

* For performance planning, GitLab publishes a set of prescriptive reference architectures. Our standard reference architectures begin at 2,000 users for Cloud Native Hybrid deployments.
  * The Customer XYZ currently a 2K Cloud Native Hybrid Environment  which is not an optimal setup to meet their performance goals since Customer XYZ is planning to add additional users to the environment. . This could be the cause of  performance degradation and support issues.

### Recommendation

* GitLab recommends Customer XYZ to move to 3K Cloud Native Hybrid Deployment with External Gitaly. This would allow Customer XYZ to scale the environment with additional users without degrading performance and user experience. 

## Recommended Target Architecture

* The tables and diagram below illustrate the 3K hybrid environment, using formats consistent with those used for the typical environment described earlier.
Below are components deployed in Kubernetes. These are distributed across multiple node groups. You have flexibility in adjusting the overall configuration, provided you adhere to the specified minimum CPU and memory requirements."

| Component Node Group | Target Node Pool Totals |AWS Example |
| ---      | ---      | ---      |
| Webservice   | 16 vCPU, 20 GB memory (request), 28 GB memory (limit) | 2 x c5.4xlarge   |
| Sidekiq | 7.2 vCPU, 16 GB memory (request), 32 GB memory (limit) | 3 x m5.xlarge |
| Supporting Services   | 4 vCPU, 15 GB memory |2 x m5.large  |

* Following this are the backend components deployed on dedicated Linux-based virtual machines (VMs) using the Linux package. In some cases, these may alternatively utilize external Platform-as-a-Service (PaaS) offerings where appropriate

| Service | Nodes | Configuration | AWS Example |
| ---      | ---      | ---      | ---      |
| Internal LB   | 1  | 4 vCPU, 3.6 GB memory   | c5n.xlarge |
| Gitaly | 3 | 4 vCPU, 15 GB memory6 | m5.xlarge |
| Prefect | 3 | 2 vCPU, 1.8 GB memory | c5.large |
| Object Storage | - | - | - |

* AWS Managed external [PostgreSQL PaaS solutions (AWS RDS)](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#provide-your-own-postgresql-instance
) will be used for the GitLab and Praefect DB.

* AWS Managed external Redis PaaS solutions (Elasticache) will be used for caching and sidekiq job processing. Refer to [this link](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#provide-your-own-redis-instance
) for details.

* It's advisable to employ a reliable third-party load balancer or service (LB PaaS) capable of high availability (HA). Note that sizing requirements vary based on the chosen load balancer and other factors like network bandwidth. For more comprehensive information, see the [section here](#go-forward-strategy).

### Backup/Restore

* GitLab Professional services reviewed Customer XYZ storage setup and backup/restore methodology. Currently Customer XYZ is leveraging Gitlab toolbox pod for performing backup of  GitLab application and database backup.

* Data included in GitLab backups falls into three categories: the GitLab application database, Git repositories, and file storage. In GitLab installations with a lot of data, the best backup strategy is often to separate out these data types and identify the best backup strategy for each category.
In the case of the GitLab database and Git repositories, there are some special considerations for the backup and restore process that are handled automatically by the GitLab backup tool. We recommend continuing to use GitLab’s backup capability for these two types of data.

* For file storage, GitLab only requires that the files be accessible at the expected path. There are many options for backup and restore of the files, and general-purpose file backup tools are often able to manage large volumes better than GitLab’s built-in capability. It can be difficult to give specific recommendations because of the range of options available. If there is a standard server backup strategy already in use at Customer XYZ, it is probably suitable for backing up the paths GitLab uses for file storage. This could range from a commercial, enterprise DR platform to a simple scheduled rsync job.
GitLab also supports the use of an S3-compatible object storage backend for file storage. We have tested GitLab against several object storage backends, and expect others to work as well. This includes open-source self-hosted implementations like MinIO, cloud object storage services, and the S3-compatible interface provided by many enterprise storage products. There are several advantages to an external object storage service: external object storage is a major requirement to move to a multi-node GitLab deployment. More immediately, object storage services often provide their own backup/restore or snapshot features that can be used to provide backup and DR capability for GitLab’s file storage.

* Once file storage is backed up by an alternate method, it can be excluded from GitLab-generated backup archives. In an event where GitLab backups are restored, the best practice is to use a file storage backup that is newer than the GitLab-generated backup. This ensures that all files referenced in the GitLab database are present. If an older backup is used, GitLab will function normally, but users may get a 404 error on files like job artifacts and uploads that were created after the file backup but before the database backup.

### Configuration

GitLab reviewed the configuration of Customer XYZ’s Production environment. The following are brief recommendations for configuration changes that may improve stability and simplify troubleshooting.

* Evaluate maxReplicas configuration , The maxReplicas setting controls the maximum number of pods that can be created by the autoscaler. While it's important for preventing resource exhaustion, it's also critical to ensure that this setting does not unnecessarily restrict the ability of your services to scale.Reassess the maxReplicas settings for each service. This will allow services to scale more freely in response to demand.

### Disaster Recovery  and High Availability

"Customer XYZ's current disaster recovery (DR) strategy relies on backup and restore procedures, which presents several limitations:

* Extended Recovery Time: In the event of an outage, spinning up a backup instance is a time-consuming, manual process compared to having a standby system ready.
* Infrequent Testing: The recovery process is not regularly tested, potentially leading to unforeseen issues during an actual disaster.
* GitLab's approach to resilient deployments distinguishes between two key concepts:
* Disaster Recovery (DR): The capability to recover the entire GitLab deployment in case of a catastrophic loss.
* High Availability (HA): A GitLab deployment with built-in redundancy to minimize the likelihood of system failure.
* For customers with high availability requirements, GitLab recommends implementing a 3k reference architecture. This configuration, when deployed on-premises, should be distributed across multiple physical machines to maximize availability.
* While the initial cost of this multi-node deployment may exceed that of a 2K setup, it's important to weigh this against the expenses and operational complexities associated with maintaining four separate GitLab instances.

### Recommendation: 

* GitLab recommends to deploy a 3K reference architecture for High availability. The 3k architecture offers a more integrated, scalable, and resilient solution that can provide both high availability and improved disaster recovery capabilities.

## Disaster Recovery Considerations

### Current State

* No hot or cold standby nodes for immediate traffic redirection
* Disaster recovery relies on manual processes
* Backups stored in S3 buckets
* Recovery requires infrastructure provisioning in a new region before restoration

### Potential Enhancements

* GitLab disaster recovery is feasible but introduces operational complexity
* Standby environments require deployment, maintenance, and ongoing management

### Key Decision Point

* The implementation of [GitLab disaster recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery) is a business consideration. It hinges on balancing acceptable downtime against the financial commitment of maintaining a standby environment.

* For a comprehensive understanding of GitLab disaster recovery implementation and its implications, refer to the [GitLab Geo technical documentation](https://docs.gitlab.com/ee/administration/geo/). These resources will aid in making an informed decision.

## Monitoring

### Current monitoring

* Customer XYZ is Primarily using Datadog and AWS EKS console to monitor the GitLab installation, logs from GitLab are forwarded to Splnk. The installation is used by approximately 800 active users, this does not seem like many users, but they are quite active (pipelines triggered etc). Although it might seem like a smaller-scale deployment, Customer XYZ requires a larger deployment due to the active nature of the GitLab users, additional users that are planned to be added and other impacting factors to support additional load.

* Although monitoring is not a concern for Customer XYZ currently,  it is recommended to leverage prometheus and  grafana to monitor important GitLab metrics.
There are several repositories available for improving the Grafana graphs of a GitLab installation, refer to the resources in the table below.

| Dashboard             | Link     | Explanation of Grafana dashboard monitoring features|
| ---                   | ---      | ---                                                 |
| GitLab GET Hybrid Environment SLO Monitoring    | https://gitlab.com/gitlab-com/runbooks/-/tree/master/reference-architectures/get-hybrid | Part of the GitLab Runbooks, this specific directory offers guidance on setting up Service Level Objective (SLO) monitoring for GET based hybrid cloud environments |
| GitLab On-call Run Books   | https://gitlab.com/gitlab-com/runbooks | This repository contains runbooks for GitLab.com, which are used for operational procedures and incident response. They provide detailed steps for managing and resolving issues with GitLab instances |
| Grafana JSON Dashboards  | https://gitlab.com/gitlab-org/grafana-dashboards | This repository hosts various Grafana dashboard configurations used by GitLab. Implement a subset of these configurations, as you see fit |
| GitLab Grafana Configuration | https://docs.gitlab.com/ee/administration/monitoring/performance/grafana_configuration.html | Provides detailed instructions on how to configure Grafana for monitoring GitLab instances. It includes steps for integrating Grafana with GitLab for advanced monitoring capabilities |
| Grafana Dashboards Management | https://grafana.com/docs/grafana/latest/dashboards/manage-dashboards/ | Offers comprehensive guidance on managing Grafana dashboards, including creation, configuration, and sharing | 
| Cloud Native Hybrid Kube Prometheus Stack | https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_advanced_monitoring.md#cloud-native-hybrid-kube-prometheus-stack | Part of the GitLab Environment Toolkit documentation, this section provides advanced monitoring setup instructions using the Cloud Native Hybrid Kube-Prometheus Stack. It's aimed at enabling sophisticated monitoring solutions for GitLab instances deployed in Kubernetes, covering installation and configuration of Prometheus, Grafana, and other monitoring tools within a cloud-native ecosystem |

## Monitoring recommendation

* We would recommend Customer XYZ to make use of existing Grafana and Prometheus code available to enhance the monitoring of GitLab environment. For example, when implementing GitLab GET Hybrid Environment SLO Monitoring (nr 1 in table 1), we already receive a comprehensive dashboard of critical GitLab features.

| Dashboard             | Link     | Explanation of Grafana dashboard monitoring features|
| ---                   | ---      | ---                                                 |
| Grafana Triage Dashboard    | https://gitlab.com/gitlab-com/runbooks/-/raw/master/reference-architectures/get-hybrid/img/grafana-triage.png   | This dashboard monitors and visualizes metrics critical for triaging issues within GitLab. It includes widgets for tracking error rates, response times, system load, and other indicators relevant for identifying and prioritizing incidents and performance bottlenecks |
| Grafana Webservice Dashboard   | https://gitlab.com/gitlab-com/runbooks/-/raw/master/reference-architectures/get-hybrid/img/grafana-webservice.png| This dashboard focuses on monitoring the health and performance of GitLab web services. It displays error rates and request rates, amongst others. This allows you to quickly identify any degradation in service |
| Grafana Sidekiq Dashboard  |  https://gitlab.com/gitlab-com/runbooks/-/raw/master/reference-architectures/get-hybrid/img/grafana-sidekiq.png       | Tailored for monitoring Sidekiq background jobs, this dashboard shows metrics such as job processing times, queue lengths, and error rates. This is essential for ensuring background tasks are processed efficiently and identifying any issues causing delays or failures in job processing.   |
| Grafana Gitaly Dashboard | https://gitlab.com/gitlab-com/runbooks/-/raw/master/reference-architectures/get-hybrid/img/grafana-gitaly.png | his dashboard is designed to monitor Gitaly, the Git RPC service for GitLab, showcasing metrics related to Git operation performance, error rates, and throughput. Such metrics are crucial for maintaining the integrity and performance of Git repositories in a GitLab environment.

## GitLab Environment Toolkit (GET)

* GitLab recommends leveraging The GitLab Environment Toolkit (GET) for Environment deployments..  Deploying GitLab using the GitLab Environment Toolkit (GET) results in a supported GitLab environment, based on GitLab's reference architectures. This can enhance performance and will be beneficial for requesting support from GitLab in the future.
refer to [this link](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_advanced_hybrid.md) for more information.

## Architecture Recommendations and GAP Analysis

### Architecture Recommendation

Based on the information and details received by the customer, GitLab advises to below options to Customer XYZ:

#### Option 1

* Migrate to an external GitLab deployment while keeping the existing architecture unchanged. This configuration will be supported, but there are several pros and cons to consider with this approach:

| Pros | Cons|
| ---      | ---      |
| East to Maintain   | No High AVailability    |
| Cost effective | Scalability constraints beyond 2K users/ 40 RPS |
| Easier upgrades   |          |

#### Option 2

* Move to the external Gitaly cluster following the reference 3K Cloud Native Hybrid Reference architecture

| Pros | Cons|
| ---      | ---      |
| Inbuild High Availability    | 5+ additional nodes to set up (Gitlay + Praefect)    | 
| Easily scalable to 3K users | Management overhead due to higher machine/nodes footprint  |
| Supports up to 60 RPS   |  Higher Cost compared to 2K environment  |
|             | Upgrade will take more time due to increase number of nodes |

* GitLab recommends that Customer XYZ move to an external Gitaly or Gitaly Cluster deployment, as this configuration will receive the best support from GitLab. As a general rule, the more a user's architecture deviates from GitLab's supported reference architectures, the more challenging it can be to receive support when issues arise. Any architectural changes introduce an additional layer of complexity, which can make it harder to pinpoint the root cause of potential problems.

* If high availability (HA) is the primary concern for Customer XYZ, GitLab's 3K standard reference architecture is highly recommended.

* Once Customer XYZ moves to a supported architecture, the next logical step is to leverage GitLab Geo for disaster recovery (DR) support, which would likely be the optimal choice. This supported configuration will provide the best balance of HA, reliability, Disaster Recovery and ease of support from GitLab.

### GAP Analysis

* To transition from the current non-high availability (non-HA) architecture to GitLab's recommended 3K reference architecture, several changes need to be made:
The Kubernetes cluster and backend stateful components (such as databases) will need to be scaled up to handle the increased user and system load.
The Gitaly cluster will need to be scaled out by adding more nodes. This is necessary to support the additional load on the Git repository storage and processing components.
* These scaling adjustments to the Kubernetes infrastructure and Gitaly cluster are required in order to migrate from the existing non-HA setup to the more robust 3K reference architecture, which is designed to provide high availability and improved performance at scale


* Below tables describe resource difference/GAP between the current and target architecture. 

![alt text](<./img/Screenshot_2024-09-05_at_11.22.20_AM.png>)
![alt text](<./img/Screenshot_2024-09-05_at_11.22.35_AM.png>)

### Notes

* 1) AWS Managed services (ie. ELB, RDS, Elasticache, and S3) are not added to the above table. When using Managed services (PaaS) solutions, it's best practice to set up a minimum of three nodes in three separate availability zones. This is aligned with the principles of resilient cloud architecture, as it helps ensure high availability and fault tolerance in the event of a failure in any one zone.
* 2) The table provided offers a comparative analysis of the components and resources between Customer XYZ's current GitLab architecture and GitLab's 3K Reference Architecture.
For the purposes of this analysis, we have made an assumption regarding the Gitaly component. As there is an ongoing initiative to externalize Gitaly, we have treated it as if it were already running externally in our current evaluation.
This comparison allows us to clearly see how Customer XYZ's existing setup aligns with or differs from the recommended 3K Reference Architecture across various components and resource allocations. It provides a baseline for understanding where adjustments might be needed to optimize performance and align more closely with GitLab's recommendations.
* 3) Customer XYZ's current GitLab implementation exceeds the specifications of the 3K GitLab reference architecture in most aspects, with the notable exception of the Gitaly Cluster component. It's important to understand that GitLab's Reference Architectures are designed as a starting point and offer flexibility for scaling and customization.

GitLab's recommendation for Customer XYZ is to fine-tune the environment post-externalize Gitlay to address specific needs, which may include enhancing performance capacity or optimizing costs. This approach is both expected and encouraged.
The scaling process can be approached in two ways:

* Iterative scaling: Gradually adjusting individual components as needed.
* Wholesale scaling: Moving to the next tier of reference architecture.
The choice between these approaches should be guided by performance metrics. When these metrics indicate that a particular component is nearing its capacity limits, it signals the need for scaling. This flexible approach ensures that Customer XYZ's GitLab environment can continue to meet evolving demands efficiently and cost-effectively.

## Go Forward Strategy

To address the challenges in Customer XYZ's GitLab deployment, we propose a phased approach for re-architecting the environment. This strategy allows for incremental improvements in performance and maintainability, making the transition more manageable. Below are the recommendation from GitLab PS:

* Transition to 3K Cloud Native Hybrid Deployment GitLab recommends that Customer XYZ migrate to a 3K Cloud Native Hybrid Deployment with External Gitaly. This architecture will enable Customer XYZ to accommodate user growth while maintaining high performance and a positive user experience.

* Implement GitLab Environment Toolkit (GET) For future deployments and upgrades, adopt the GitLab Environment Toolkit (GET). This tool ensures alignment with GitLab's reference architectures, potentially boosting performance and streamlining support interactions with GitLab.

* Establish Regular Performance Testing Implement a routine of running the GitLab Performance Tool (GPT). This practice helps maintain consistent performance levels and ensures the GitLab instance continues to meet Customer XYZ's evolving requirements over time.

* Enhance Monitoring Capabilities Improve system oversight by implementing GitLab's Prometheus and Grafana dashboards. This will provide more comprehensive and proactive monitoring of the GitLab environment.

* Deploy GitLab Geo in near term for disaster recovery offers several key benefits to customers:
  * Business continuity: Geo provides a fully operational secondary site that can take over quickly if the primary site fails, minimizing downtime.
  * Data protection: It maintains a replicated copy of your GitLab data, including repositories, issues, and other metadata, safeguarding against data loss.
  * Geographic distribution: Geo allows for distributed teams to work from local sites, improving performance for geographically dispersed users.
  * Load balancing: The secondary site can serve read-only traffic, reducing load on the primary site.
  * Compliance: For organizations with strict data residency requirements, Geo enables data replication across different regions.
  * Faster recovery: In case of a disaster, failover to the secondary site can be much quicker than restoring from backups.
  * Reduced downtime risk: Regular replication ensures the secondary site is always up-to-date, minimizing potential data loss during a failover.
  * Easier maintenance: Geo allows for performing upgrades and maintenance on the secondary site before switching it to primary, reducing overall system downtime.
* By following this phased approach, Customer XYZ can systematically improve its GitLab deployment, addressing current issues while preparing for future growth and maintaining optimal performance.

## References

* [GitLab Performance Tool:](https://gitlab.com/gitlab-org/quality/performance) Performance test any GitLab instance
* [GitLab SOS:](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) Gather GitLab logs from system
* [Fast-stats:](https://gitlab.com/gitlab-com/support/toolbox/fast-stats) Create graphs and compare performance statistics from data within GitLab logs
* [GitLab Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/)
* [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit)
* [GitLab GEO](https://docs.gitlab.com/ee/administration/geo/)
