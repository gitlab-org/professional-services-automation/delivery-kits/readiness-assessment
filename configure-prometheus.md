[[_TOC_]]

# HAProxy Exporter

## Steps to setup scraping for an HAProxy node:

### On the HAProxy node:

```
# Compile HAProxy for Prometheus
sudo apt update
sudo apt install -y git ca-certificates gcc libc6-dev liblua5.3-dev libpcre3-dev libssl-dev libsystemd-dev make zlib1g-dev
sudo git clone https://git.haproxy.org/git/haproxy-2.1.git /tmp/haproxy
sudo make -C /tmp/haproxy TARGET=linux-glibc EXTRA_OBJS="contrib/prometheus-exporter/service-prometheus.o"
sudo make -C /tmp/haproxy install-bin

# Setup HAProxy for server
sudo mkdir -p /etc/haproxy
sudo mkdir -p /var/lib/haproxy 
sudo touch /var/lib/haproxy/stats
sudo cp /usr/local/sbin/haproxy /usr/sbin/haproxy
sudo cp /tmp/haproxy/examples/haproxy.init /etc/init.d/haproxy
sudo chmod 755 /etc/init.d/haproxy
sudo systemctl daemon-reload
sudo chkconfig haproxy on
haproxy -vv # Check

# Configure load-balancer
sudo vim /etc/haproxy/haproxy.cfg
```

```
global
   log /dev/log local0
   log /dev/log local1 notice
   chroot /var/lib/haproxy
   stats timeout 30s
   user haproxy   # if `sudo useradd -r haproxy`
   group haproxy
   daemon

defaults
   log global
   mode http
   option httplog
   option dontlognull
   timeout connect 5000
   timeout client 50000
   timeout server 50000

frontend http-in
   bind *:80
   acl grafana path_beg -i /-/grafana
   use_backend grafana if grafana

   default_backend gitlab-rails

frontend prometheus
   bind *:9090

   default_backend prometheus

backend gitlab-rails
   option httpchk GET /-/readiness
   server <gitlab-rails-hostname> <private_ip>:80 check

frontend stats
   bind *:8404
   option http-use-htx
   http-request use-service prometheus-exporter if { path /metrics }
   stats enable
   stats uri /stats
   stats refresh 10s

backend grafana
   option httpchk GET /-/grafana/api/health

   server grafana <monitor_node_private_ip>:80 check

backend prometheus
   option httpchk GET /-/healthy

   server prometheus <monitor_node_private_ip>:9090 check
```

```
sudo systemctl enable haproxy
sudo systemctl restart haproxy
```

### On the monitoring node:

```
sudo vim /etc/gitlab/gitlab.rb
```

```
prometheus['scrape_configs'] = [
  {
    'job_name': '<haproxy-scrape-name>',
    'static_configs' => [
      'targets' => %w(<internal_ip_1>:8404 <internal_ip_2>:8404 <internal_ip_n>:8404)
    ]
  }
]
```

```
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart
```

### Target

Confirm scrape on `<prometheus_url>/target`

![HAProxy scrape](./img/ha_proxy_scrape.png)

### References

* http://www.haproxy.org
* https://github.com/prometheus/haproxy_exporter
* https://www.haproxy.com/blog/haproxy-exposes-a-prometheus-metrics-endpoint/
* https://upcloud.com/community/tutorials/haproxy-load-balancer-centos/

# Node Exporter

## Steps to setup scraping for an NFS node:

### On the NFS node:

```
cd /tmp
curl -LO https://github.com/prometheus/node_exporter/releases/download/v0.18.1/node_e -O /tmp/node_exporter.tar.gz # latest stable release
tar -xvf /tmp/node_exporter.tar.gz -C /tmp
sudo mv /tmp/node_exporter-0.18.1.linux-amd64/node_exporter /usr/local/bin/
sudo useradd -rs /bin/false node_exporter
sudo vim /etc/systemd/system/node_exporter.service
```

```
[Unit]
Description=Node Exporter
After=network.target
 
[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter
 
[Install]
WantedBy=multi-user.target
```

```
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl status node_exporter
sudo systemctl enable node_exporter
```

### On the monitoring node:

```
sudo vim /etc/gitlab/gitlab.rb
```

```
prometheus['scrape_configs'] = [
  {
    'job_name': '<nfs-scrape-name>',
    'static_configs' => [
      'targets' => %w(<internal_ip_1>:9100 <internal_ip_2>:9100 <internal_ip_n>:9100)
    ]
  }
]
```

```
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart
```

### Target
Confirm scrape on `<prometheus_url>/target`

![NFS scrape](./img/nfs_scrape.png)

### References

* https://github.com/prometheus/node_exporter
* https://devopscube.com/monitor-linux-servers-prometheus-node-exporter/

# Prometheus

## Storage and retention

```
needed_disk_space = retention_time_seconds * ingested_samples_per_second * bytes_per_sample
```

Something to keep in mind and potentially add to the monitoring node's `gitlab.rb` file regarding Prometheus monitoring data [storage](https://prometheus.io/docs/prometheus/latest/storage/):

```
prometheus['flags'] = {
  'storage.tsdb.path' => "/var/opt/gitlab/prometheus/data",   # default
  'storage.tsdb.retention.time' => "<no_of_days>d",   # 15d by default
  'config.file' => "/var/opt/gitlab/prometheus/prometheus.yml"   # default
}
```

**NOTE:** The running configuration with all default values may be found on the Prometheus UI *Status -> Configuration* page (`http://<mon_node_ip>:9090/config`).

## Variables and Labels

- Grafana creates it’s own global variables based on Prometheus metrics exposed labels
- Prometheus labels come from several places and aggregate into a list (renaming happens in the process if necessary)
    - Exporter specific labels (e.g. `server`, `backend`, `process`, `frontend` for haproxy-exporter)
    - Prometheus default labels - `instance` and `job`
    - Service discovery labels
        - Static - in individual *scrape_configs/static_configs/labels* in the `prometheus.yml` file
        - Dynamic - created as part of `prometheus.yml` *consul_sd_configs*
    - External labels - *global/external_labels* list in `prometheus.yml`
      - Only these are not available for `alertmanager.yml` recordings, although the labels are injected with the metric when Prometheus is pushing alerts to the Alertmanager

## Rules

GitLab Omnibus always creates (or overwrites) the following rules files:
- `/var/opt/gitlab/prometheus/rules/gitlab.rules`
- `/var/opt/gitlab/prometheus/rules/node.rules`

So upon reconfigure any changes to the rules files :point_up: will be lost, unless they are configured in the monitoring node's `gitlab.rb` file under a different path (or extension), as following:

```
prometheus['rules_files'] = ['/var/opt/gitlab/prometheus/rules/*.yml']
```

This means GitLab Omnibus will create its default `*.rules` files but Prometheus will only use `*.yml` files for the rules, thus allowing to deploy them as part of IaC scripts.

Example of configuring `*.yml` for the rules path and running `gitlab-ctl reconfigure`:

```
$ sudo ls -lah /var/opt/gitlab/prometheus/rules/
total 80K
drwxr-x--- 2 gitlab-prometheus root  139 May  6 19:32 .
drwxr-x--- 4 gitlab-prometheus root   79 May  6 19:32 ..
-rw-r--r-- 1 gitlab-prometheus root  18K May  6 19:32 gitlab.rules
-rw-r--r-- 1 gitlab-prometheus root  18K May  3 13:27 gitlab.yml
-rw-r--r-- 1 gitlab-prometheus root 7.0K May  5 20:14 haproxy.yml
-rw-r--r-- 1 gitlab-prometheus root 1.8K May  6 19:32 node.rules
-rw-r--r-- 1 gitlab-prometheus root 1.8K May  6 17:14 node.yml
-rw-r--r-- 1 gitlab-prometheus root  19K May  5 20:27 prometheus.yml
```

**NOTE**: Make sure the `*.yml` files have `gitlab-prometheus:root` permissions.

## Registry metrics

Despite the confusing wording (debug) the [Registry metrics](https://docs.gitlab.com/ee/administration/monitoring/prometheus/registry_exporter.html) are by default enabled and used on gitlab.com.

The following line should be added to the App (Frontend) node's `gitlab.rb` file:

```
registry['debug_addr'] = "localhost:5001"
```

Apart from adding the config :point_up: and exposing the port, in case of an external Prometheus server (monitoring node), it's `gitlab.rb` file will need the following additional scrape config:

```
prometheus['scrape_configs'] = [
  {
    'job_name': '<registry-scrape-name>',
    'static_configs' => [
      'targets' => %w(<internal_ip_1>:5001 <internal_ip_2>:5001 <internal_ip_n>:5001)
    ]
  }
]
```

This will populate the monitoring node's `prometheus.yml` file upon `gitlab-ctl reconfigure` and scrape the registry's `<ip>:<port>`.

**NOTE**: The registry doesn’t report metrics that return `0`. After you hit the registry endpoint at least the following metrics should populate:
- `registry_http_requests_total`
- `registry_http_request_duration_seconds_bucket`
- `registry_storage_cache_total`

## GitLab exporter metrics

After [enabling the metrics](https://docs.gitlab.com/ee/administration/monitoring/prometheus/gitlab_exporter.html) on the Redis/DB nodes you still need to add the following scrape config to the monitoring node's `gitlab.rb` file:

```
prometheus['scrape_configs'] = [
  {
    'job_name': '<gitlab-exporter-scrape-name>',
    'static_configs' => [
      'targets' => %w(<internal_ip_1>:9168 <internal_ip_2>:9168 <internal_ip_n>:9168)
    ]
  }
]
```

## Testing and Checking

Use the [`promtool`](https://github.com/prometheus/prometheus/tree/master/cmd/promtool) (binary that comes with Prometheus) to test and check rules.

Test rules:

```
# For a single test file.
./promtool test rules test.yml

# If you have multiple test files, say test1.yml,test2.yml,test2.yml
./promtool test rules test1.yml test2.yml test3.yml
```

Check rules:

```
go get github.com/prometheus/prometheus/cmd/promtool
promtool check rules /path/to/example.rules.yml
```

## Rule files

[consul.yml](uploads/13ff1ed72072df5f5dcbfc8150fa64ed/consul.yml)

[gitaly.yml](uploads/3895c2ab891c044cf9b02b50c8a062b6/gitaly.yml)

[haproxy.yml](uploads/5762122574d727787b7130947b061429/haproxy.yml)

[nginx.yml](uploads/6c10fe03bf6ebb4576e21f18bee69350/nginx.yml)

[node.yml](uploads/52672de61dc6039e12dc7466607d7e4e/node.yml)

[pgbouncer.yml](uploads/003e1b52785c00eb0519eb1e0ca6be1a/pgbouncer.yml)

[postgresql.yml](uploads/844f8df9dc52bbf0a781bb540b309b57/postgresql.yml)

[prometheus.yml](uploads/4296131c70f36e026cca7b184ed0ad6d/prometheus.yml)

[redis.yml](uploads/6df0575b26788d1ce16dd8dac4e282ef/redis.yml)

[sidekiq.yml](uploads/a41ba86726f289700c79db950f6739d7/sidekiq.yml)

[workhorse.yml](uploads/f157d765af88e0987043e47e8b2bf0db/workhorse.yml)

## References

- [GitLab Omnibus `gitlab.rb` template](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template)
- [Prometheus official docs](https://prometheus.io/docs/introduction/overview/)
- [Default port allocations](https://github.com/prometheus/prometheus/wiki/Default-port-allocations)
- [`prometheus.rb` source code - cookbook](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/f0105a9574d3bab6cdfb533301c5f14ea326d4dc/files/gitlab-cookbooks/monitoring/libraries/prometheus.rb)
- [Unit testing for rules](https://prometheus.io/docs/prometheus/latest/configuration/unit_testing_rules/)
- [Testing Prometheus alerts](https://zerokspot.com/weblog/2019/11/25/testing-prometheus-alerts/)

# Alert Manager

## Configuration

The following additional configuration should go into the monitoring node's `gitlab.rb` file:

```
################################################################################
## Prometheus Alertmanager
################################################################################
alertmanager['enable'] = true
alertmanager['flags'] = {
  'web.listen-address' => '<mon_node_ip>:9093',   # exposes the Alertmanager UI
  'storage.path' => '/var/opt/gitlab/alertmanager/data',
  'config.file' => '/var/opt/gitlab/alertmanager/alertmanager.yml'
}
alertmanager['listen_address'] = '<mon_node_ip>:9093'
alertmanager['global'] = {
  'http_config' => {   # if needed due to proxy
    'proxy_url' => '{{ proxy_url }}'
  },
  'smtp_from' => '<address>',   # Alertmanager copy of SMTP configuration
  'smtp_smarthost' => '<host>:587',
  'smtp_auth_username' => '<username>',
  'smtp_auth_password' => '<pwd>',
  'smtp_require_tls' => true,
  'slack_api_url' => 'https://hooks.slack.com/services/xxx/yyy/zzz'
}
alertmanager['templates'] = '/var/opt/gitlab/alertmanager/notifications.tmpl'   # see Slack notifications template
alertmanager['routes'] = [{
  'match_re' => {
    'severity' => '<custom-alert-levels>'   # e.g. error, warning, s1, etc.
  },
  'receiver' => 'slack-receiver'
}]
alertmanager['receivers'] = [{
  'name' => 'email-receiver',
  'email_configs' => [
    'to' => '<email>',
    'send_resolved' => true
  ]},
  {'name' => 'slack-receiver',
  'slack_configs' => [
    'channel' => '#<channel>',
    'send_resolved' => true,
    'icon_url' => 'https://avatars3.githubusercontent.com/u/3380462',
    'title' => '{{ template "custom_title" . }}',   # see Slack notifications template
    'text' => '{{ template "custom_slack_message" . }}'
  ]}
]
# Note: declare a custom-receiver because default-receiver forces require_tls (incompatible with mailcatcher)
alertmanager['default_receiver'] = 'slack-receiver'
#alertmanager['inhibit_rules'] = '/var/opt/gitlab/prometheus/rules/*_inhibit.rules'   # if needed
```

**NOTE:** The running configuration with all default values may be found on the Alertmanager UI *Status* page (`http://<mon_node_ip>:9093/#/status`).

## Slack notifications template

```
{{ define "custom_title" }}
[{{ .Status | toUpper }}{{ if eq .Status "firing" }}:{{ .Alerts.Firing | len }}{{ end }}] {{ .CommonLabels.alertname }} for {{ .CommonLabels.job }}
{{- if gt (len .CommonLabels) (len .GroupLabels) -}}
  {{" "}}(
  {{- with .CommonLabels.Remove .GroupLabels.Names }}
    {{- range $index, $label := .SortedPairs -}}
      {{ if $index }}, {{ end }}{{- $label.Name }}="{{ $label.Value -}}"{{ end }}{{ end }})
{{ end }}
{{ end }}
{{ define "custom_slack_message" }}
{{ range .Alerts }}
*Alert:* {{ .Annotations.title }}{{ if .Labels.severity }} - `{{ .Labels.severity }}`{{ end }}
*Description:* {{ .Annotations.description }}
*Details:*
  {{ range .Labels.SortedPairs }} • *{{ .Name }}:* `{{ .Value }}`
  {{ end }}
{{ end }}
{{ end }}
```

## SMTP configuration

The following additional configuration should go into the monitoring node's `gitlab.rb` file:

```
#######################################
###               SMTP              ###
#######################################
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = '<address>'
gitlab_rails['smtp_port'] = '<port>'
gitlab_rails['smtp_user_name'] = '<username>'
gitlab_rails['smtp_password'] = '<pwd>'
gitlab_rails['smtp_domain'] = '<domain>'
gitlab_rails['smtp_tls'] = true
```

## References

- [Runbook rules](https://gitlab.com/gitlab-com/runbooks/tree/master/rules)
- [Prometheus/Exporters/Alertmanager/Grafana setup](https://ashish.one/blogs/)
- [Alertmanager source code](https://github.com/prometheus/alertmanager)
- [`alertmanager.yml` example](https://github.com/prometheus/alertmanager/blob/master/doc/examples/simple.yml)
- [Alertmanager default source code template - cookbook](https://gitlab.com/gitlab-cookbooks/gitlab-alertmanager/-/blob/master/attributes/default.rb)
- [Awesome Prometheus alerts](https://awesome-prometheus-alerts.grep.to)
- [Step-by-step guide to setting up Prometheus Alertmanager with Slack, PagerDuty, and Gmail](https://grafana.com/blog/2020/02/25/step-by-step-guide-to-setting-up-prometheus-alertmanager-with-slack-pagerduty-and-gmail/)
- [Better Slack alerts from Prometheus](https://medium.com/quiq-blog/better-slack-alerts-from-prometheus-49125c8c672b)

# Grafana

Note - As of [GitLab 16.3](https://docs.gitlab.com/ee/update/deprecations.html#gitlab-163), the version of Grafana bundled with Omnibus GitLab is deprecated and has been removed. For a quick (non-production grade) Grafana installation to view metrics e.g. for additional data during a load test or analysis, see the [tools/grafana](tools/grafana) directory for a simple docker-compose stack that may be used as a starting point.


## Deploying dashboards

To deploy additional Grafana dashboards (apart from the default GitLab Omnibus) add and extend the following in the monitoring node's `gitlab.rb` file:

```
grafana['dashboards'] = [
  {
    'name' => 'GitLab Omnibus',
    'orgId' => 1,
    'folder' => 'GitLab Omnibus',
    'type' => 'file',
    'disableDeletion' => true,
    'updateIntervalSeconds' => 600,
    'options' => {
      'path' => '/opt/gitlab/embedded/service/grafana-dashboards',
    }
  },
  {
    'name' => '<dashboard_name>',
    'orgId' => 2,   # increment ID
    'folder' => '<dashboard_folder>',
    'type' => 'file',
    'disableDeletion' => true,
    'updateIntervalSeconds' => 600,
    'options' => {
      'path' => '<path_to_json_files_folder>',
    }
  }
]
```

This should allow you to place your Grafana `.json` files to `<path_to_json_files_folder>` via IaC scripts.

GitLab Omnibus (default) dashboards:

```
$ sudo ls -lah /opt/gitlab/embedded/service/grafana-dashboards/
total 156K
drwxr-xr-x  2 root root 202 Apr  6 19:50 .
drwxr-xr-x 10 root root 165 Apr  6 19:50 ..
-rw-r--r--  1 root root 14K Mar 26 14:38 gitaly.json
-rw-r--r--  1 root root 12K Mar 26 14:38 nginx.json
-rw-r--r--  1 root root 22K Mar 26 14:38 overview.json
-rw-r--r--  1 root root 21K Mar 26 14:38 postgresql.json
-rw-r--r--  1 root root 12K Mar 26 14:38 rails-app.json
-rw-r--r--  1 root root 542 Mar 26 14:38 README.md
-rw-r--r--  1 root root 25K Mar 26 14:38 redis.json
-rw-r--r--  1 root root 12K Mar 26 14:38 registry.json
-rw-r--r--  1 root root 22K Mar 26 14:38 service_platform_metrics.json
```

## Sharing dashboards

- Set each (dashboards) panels `Data Source` to `default` before exporting a dashboard JSON to avoid conflicts on import.
- Dashboards loaded from the filesystem cannot be altered via the UI. You have to deploy the saved JSON (exported from the UI) with the changes.
- **BUG:** Importing a dashboard will clear some of the the global variables fields in the UI.

## Dashboard files

[HAProxy-1589401905950.json](uploads/31281f0ed820ae7fa8c57d1bdd4f8e9e/HAProxy-1589401905950.json)

[Host_Stats_GitLab.com-1589322475192.json](uploads/616fca94e4ac7512f57829315ec5c1c9/Host_Stats_GitLab.com-1589322475192.json)

[Node_-_iostat_GitLab.com-1589325190546.json](uploads/cdb41023ba5aa6b9f0cb6416becb8833/Node_-_iostat_GitLab.com-1589325190546.json)

[Node_Exporter_Full_public-1589401380549.json](uploads/881531f120b6aee9bbdfbdece9234de2/Node_Exporter_Full_public-1589401380549.json)

[Node_Exporter_GitLab.com-1589324303195.json](uploads/309631aafe37c3e5e411e6f1a79ed2eb/Node_Exporter_GitLab.com-1589324303195.json)

[Node_Exporter_Server_Metrics_GitLab.com-1589323675657.json](uploads/2c5188013bfb003656c1d09edee517f2/Node_Exporter_Server_Metrics_GitLab.com-1589323675657.json)

[Node_Exporter_Server_Metrics_public-1589401420051.json](uploads/306ac5dd6e24840cb2e9e0c7c788e661/Node_Exporter_Server_Metrics_public-1589401420051.json)

[Node_Network_GitLab.com-1589323045317.json](uploads/4a5cfd8ea91a262380df5cbcfcf76494/Node_Network_GitLab.com-1589323045317.json)

[Prometheus_Performance_GitLab.com-1589325506188.json](uploads/cd13aa8bd16bf8a81e9148cf3c171cc0/Prometheus_Performance_GitLab.com-1589325506188.json)

[Git_storage_over_time_GitLab.com-1588880936099.json](uploads/70b6b0384688bc0b91c000dfd4cf5257/Git_storage_over_time_GitLab.com-1588880936099.json)

[HAProxy_gitlab.com-1588693297076.json](uploads/7afd8c6662a15c47a95ec7cba8dc3a2d/HAProxy_gitlab.com-1588693297076.json)

[NFS_mountstats_GitLab.com-1588877382511.json](uploads/d82f9e8317bb0a31c54505abe3ea3f14/NFS_mountstats_GitLab.com-1588877382511.json)

[NFS-1588877338722.json](uploads/e448c8a7d36b531ba5efad52217b2bbb/NFS-1588877338722.json)

[Nginx-1588888805907.json](uploads/3027c714ea916fd9dd22675e07b6691d/Nginx-1588888805907.json)

[Nodes-1588880920905.json](uploads/cf0936ae6f901cf3bcc88d983ba26669/Nodes-1588880920905.json)

# Misc

## Enable Prometheus (Rails) metrics

The metrics are not enabled by default and there is currently no `gitlab.rb` configuration to do so.

> The `puma/unicorn` and `sidekiq` metrics can be enabled from the `gitlab.rb`, as well as the `gitlab_exporter` metrics, but not the ones on the rails `/-/metrics` endpoint.

To avoid having to manually [enable them from the UI](https://docs.gitlab.com/ee/administration/monitoring/prometheus/gitlab_metrics.html):

![Prometheus metrics](./img/metrics_prometheus.png)

The [Application API settings](https://docs.gitlab.com/ee/api/settings.html#list-of-settings-that-can-be-accessed-via-api-calls) expose the `prometheus_metrics_enabled` field.

By running the following you can programmatically enable the Prometheus (Rails) metrics:

```
curl --request PUT \
     --header "PRIVATE-TOKEN: <admin_personal_access_token>" \
     https://<domain>/api/v4/application/settings?prometheus_metrics_enabled=true
```

[Restart GitLab](https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-restart) for the changes to take effect.

# New features

- [Custom metrics available in Core](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/#custom-metrics-available-in-core)
- [Alerting available in GitLab Core](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/#alerting-available-in-gitlab-core)
