# Quick Grafana Setup

As the bundled Grafana has been removed from GitLab v16.3 onwards, this directory contains a simple docker-compose stack that a customer/GitLab engineer may temporarily deploy to gather additional system-level and GitLab component metrics to support a Load Test or other system analysis. 

For a 1k install, all that is required is to enable the Prometheus service and make it accessible to the host running the Grafana stack. For other environments/additional node types, some additional configuration may be required - see [configure-prometheus.md](/configure-prometheus.md) document in this repository.

A subset of dashboards from https://gitlab.com/gitlab-org/grafana-dashboards/-/tree/master/omnibus?ref_type=heads has been configured along with a Node Exporter dashboard for system-level metrics. Note that these have been modified to work correctly under newer versions of Grafana e.g. switching to `$__rate_interval` (see https://grafana.com/blog/2020/09/28/new-in-grafana-7.2-__rate_interval-for-prometheus-rate-queries-that-just-work/)

## Requirements

* A host able to run `docker compose` which also has network connectivity to the GitLab instances
* GitLab Prometheus enabled and configured to listen on the local network

### GitLab Prometheus

Prometheus needs to be enabled on the Omnibus/monitoring node (it is on by default - see https://docs.gitlab.com/ee/administration/monitoring/prometheus/), and should be reachable from the Grafana host. The default configuration is to only listen on localhost, so the Prometheus endpoint will not be network-reachable. To modify this, add the following to `/etc/gitlab/gitlab.rb`:

```ruby
prometheus['listen_address'] = '0.0.0.0:9090'
```

And run a `sudo gitlab-ctl reconfigure` command from the shell. Note that the Prometheus endpoint is en-encrypted and does not utilise any authentication; it is recommended that appropriate firewall rules or other security measures are made to restrict access.

## Setup

On a host with Docker (and compose extension) installed, change into this directory and edit the [grafana-provisioning/datasources/datasources.yml](grafana-provisioning/datasources/datasources.yml) and update it with the address of your Omnibus/Monitor node, e.g.

```yaml
# config file for provisioning datasources
apiVersion: 1
datasources:
  - name: GitLab Omnibus
    type: prometheus
    url: http://gitlab.example.com:9090
    access: proxy
```

And start the compose stack with:

```bash
docker compose up -d
```

You can then log into Grafana at http://your-docker -host:3000, username/password `admin` (you will be prompted to change your password). A set of dashboards have been configured and installed including Node Exporter and various GitLab metrics:

![images/summary.png](images/summary.png)
![images/node-exporter.png](images/node-exporter.png)
![images/sli.png](images/sli.png)



