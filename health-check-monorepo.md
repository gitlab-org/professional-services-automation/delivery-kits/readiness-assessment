# Mono repo Health Check guidelines and recommendations Custom SOW

## Delivery Approach

1. [ ] To get insights into the current state of monorepo, it's recommended to get the git sizer report. git-sizer computes various size metrics for a local Git repository, flagging those that might cause you problems or inconvenience. a sample output looks like this:

    Processing blobs: 1857327
    Processing trees: 37410819
    Processing commits: 11583574
    Matching commits to trees: 11583574
    Processing annotated tags: 3630
    Processing references: 10945556
    | Name                         | Value     | Level of concern               |
    | ---------------------------- | --------- | ------------------------------ |
    | Overall repository size      |           |                                |
    | * Commits                    |           |                                |
    |   * Count                    |  11.6 M   | ***********************        |
    |   * Total size               |  4.84 GiB | ********************           |
    | * Trees                      |           |                                |
    |   * Count                    |  37.4 M   | ************************       |
    |   * Total size               |   168 GiB | !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! |
    |   * Total tree entries       |  4.14 G   | !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! |
    | * Blobs                      |           |                                |
    |   * Count                    |  1.86 M   | *                              |
    |   * Total size               |   116 GiB | ************                   |
    | * Annotated tags             |           |                                |
    |   * Count                    |  3.63 k   |                                |
    | * References                 |           |                                |
    |   * Count                    |  10.9 M   | !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! |
    |     * Branches               |  25.4 k   | *                              |
    |     * Tags                   |  9.93 k   |                                |
    |     * Other                  |  10.9 M   | !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! |
    |                              |           |                                |
    | Biggest objects              |           |                                |
    | * Commits                    |           |                                |
    |   * Maximum size         [1] |   270 KiB | *****                          |
    |   * Maximum parents      [2] |     6     |                                |
    | * Trees                      |           |                                |
    |   * Maximum entries      [3] |  7.44 k   | *******                        |
    | * Blobs                      |           |                                |
    |   * Maximum size         [4] |   328 MiB | !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! |
    |                              |           |                                |
    | History structure            |           |                                |
    | * Maximum history depth      |   261 k   |                                |
    | * Maximum tag depth      [5] |     1     |                                |
    |                              |           |                                |
    | Biggest checkouts            |           |                                |
    | * Number of directories  [6] |  41.9 k   | ********************           |
    | * Maximum path depth     [7] |    17     | *                              |
    | * Maximum path length    [8] |   195 B   | *                              |
    | * Number of files        [6] |   131 k   | **                             |
    | * Total size of files    [9] |  3.19 GiB | ***                            |
    | * Number of symlinks         |     0     |                                |
    | * Number of submodules  [10] |     2     |                                |


2. [ ] Make sure to review the findings with the customer, in case where large artifacts/binary files are stored in the repositories, it is recommended to move them externally to [git large file storage (LFS)](https://docs.gitlab.com/ee/topics/git/lfs/).

3. [ ] Review the current CPU and Memory utilization of Gitaly nodes using the inbuild Prometheus monitoring service. In Monorepos Gitaly nodes are mostly busy since they provide rpc access to git repositories. make sure the CPU and Memory utilization of Gitly nodes is less than 70% on average. if nodes are constantly under resource constraints, add additional resources to bring the utilization down. 

4. [ ] The most intensive operation in git in general and specifically for monorepos is the “git-pack-objects” process, responsible for figuring out all the commit history and files and sending it back to the client The larger the repository, the more commits, files, and branches, and tags that a repository has and the more expensive this operation. To Reduce the work the server has to do for the clone and fetches, it is recommended to turn on [Gitaly packed object cache](https://docs.gitlab.com/ee/administration/gitaly/configure_gitaly.html#pack-objects-cache). Because the pack-objects cache can lead to a significant increase in disk write IO, it is off by default. In GitLab 15.11 and later, the write workload is approximately 50% lower, but the cache is still disabled by default.

5. [ ] Setting the clone depth to a small number, say 10, Git will only request the latest set of changes for a specific branch up to the desired number of commits. This approach can dramatically improve the speed of fetching changes from Git repositories, particularly if the repository has a large backlog with many big files. Essentially, this method reduces the amount of data that needs to be transferred.

6. [ ] If you can maintain a working copy of the repository on your runners, it is recommended to use git fetch instead of git clone on your CI/CD systems. The reason is that git clone fetches the entire repository from scratch, while git fetch only requests the server for references that do not already exist in the repository. This means that git fetch reduces the workload for the server. With git fetch, git-pack-objects don't have to roll up all branches and tags into a response that gets sent over. Instead, it only needs to consider a subset of references for packing up. This approach also decreases the amount of data that needs to be transferred.

### There are additional problems that come with running a Monorepo, including

1. [ ] Poor Read Distribution of the Gitaly Nodes: the [issue](https://gitlab.com/gitlab-org/gitaly/-/issues/5931#note_1875576199) below to get more context and possible troubleshooting steps that can be performed to find the root cause. Poor read distribution is a known issue with mono repo, The Gitaly team is working on a new feature “Bundle-Uri’s” which will essentially give you a balanced read distribution between the Gitaly nodes without Praefect. The feature is expected to be available behind a feature flag in version 17.1.

2. [ ] Long backup times: The traditional rails backup method doesn't scale well with the mono repo, it takes too much time to complete and is error-prone.  Some of the ways we can try to run more efficient backups for mono repo:
- Configure backup for git repositories in CNH using Rails node as a VM:

https://docs.gitlab.com/ee/administration/backup_restore/backup_large_reference_architectures.html#configure-backup-of-git-repositories

-  [Server-side backups](https://docs.gitlab.com/ee/administration/backup_restore/backup_gitlab.html#create-server-side-repository-backups) should perform better than normal backups as praefect no longer has to proxy the repository data through gRPC.

