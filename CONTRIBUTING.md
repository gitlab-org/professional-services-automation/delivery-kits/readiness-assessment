# Contributing to Implementation Services Delivery Kit
We love your input! We want to make contributing to this delivery kit as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features

## Report bugs or request features using GitLab issues
We use GitLab issues to track public bugs or request issues. Do this by [opening a new issue](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/readiness-assessment/-/issues)

## We Develop with GitLab
We use GitLab to host this delivery kit and associated automiation. We use Gitlab to track issues and feature requests, as well as accept merge requests.

## We Use [GitHub Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html#github-flow-as-a-simpler-alternative), so All Code Changes Happen Through Merge Requests.

To make your frist merge request with a proposed change: 
1. Edit the file using the WebIDE, single file editor using the GitLa UI.
1. Once you've made the changes you're proposing, commit it to a new branch and open a Merge Request to merge it into main/master. 
1. On the create MR page, add details about why you want to make the proposed change and a summary of what those changes are. 
1. Assign it to a Maintainer of the project.

## Any contributions you make will be under the CCL Software License
In short, when you submit code changes, your submissions are understood to be under the same [CCL License](/LICENSE) that covers the project. Feel free to contact the maintainers if that's a concern.


