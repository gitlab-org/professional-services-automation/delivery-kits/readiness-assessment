## GitLab Performance Tool (GPT)

[[_TOC_]]

### Preparing the Environment

On the GitLab instance:
* Create or use an existing Admin user
* Create a personal access token (PAT) with at least `api` scope permission
* Make sure the user has permission to create (top-level) groups and projects

#### VM requirements

* `docker`
* `git`
* `root` or `sudo` rights

```shell
yum install docker git -y
systemctl enable docker
systemctl start docker
git clone https://gitlab.com/gitlab-org/quality/performance.git   # gitlab.com user SSH key required
docker login registry.gitlab.com   # with the Admin user username and access token
docker pull gitlab/gpt-data-generator   # registry.gitlab.com/gitlab-org/quality/performance/gpt-data-generator
docker pull gitlab/gitlab-performance-tool   # registry.gitlab.com/gitlab-org/quality/performance/gitlab-performance-tool
```

Create folder structure and environments config file `performance/k6/config/environments/1k.json` (see [references](#references)). The `performance/k6` folder structure may be customized but we'll use it here as the example.

Adjust `1k.json` by customizing the `name`, `url` and `user` key based on the target GitLab instance.

In the `k6` folder run the following to generate test data:

```shell
cd performance/k6/
docker run -it -e ACCESS_TOKEN=xxx -v $PWD/config:/config -v $PWD/results:/results gitlab/gpt-data-generator --environment 1k.json
```
(If the above fails, you may be using podman. podman is default install on CentOs 8.x and needs specific run-as-user mount options.)

For podman, try this similar command with the Z,U mount options:
```shell
docker run -it -e ACCESS_TOKEN=xxx -v $PWD/config:/config:Z,U -v $PWD/results:/results:Z,U gitlab/gpt-data-generator --environment 1k.json
```


### Running the Tests

Create options config file `performance/k6/config/options/60s_20rps.json` (see [references](#references)).

In the same folder run the performance tests against the generated test data:

```shell
cd performance/k6/
docker run -it -e ACCESS_TOKEN=xxxx -v $PWD/config:/config -v $PWD/results:/results -v $PWD/tests:/tests gitlab/gitlab-performance-tool --environment 1k.json --options 60s_20rps.json
```

### Troubleshooting

* When running `docker run...` commands you might get:

  ```bash
  GPT data generation failed:
  POST request failed!
  Code: 403
  Response: {"message":"403 Forbidden"}
  Traceback:["/performance/lib/gpt_common.rb:39:in `make_http_request'", "/performance/lib/gpt_test_data.rb:206:in `create_group'", "./bin/generate-gpt-data:140:in `<main>'"]
  ```

  * Make sure your user has permission to create (top-level) groups
  * Navigate to `http://<env_url>/admin/users/<username>` and check that `Can create groups` is enabled. If it's not, edit the user and update this field
  * Additionally GitLab has `top_level_group_creation_enabled` feature flag which is enabled by default. However if it has explicitly been disabled on your environment, it may cause the same error. Follow [this guide](https://docs.gitlab.com/ee/administration/feature_flags.html#how-to-enable-and-disable-features-behind-flags) to enable/disable it
* `gitlab/gpt-data-generator` will use the `:latest` tag (default). `:spike`, `:test` and `<major>.<minor>.<micro>` version are also available.
* In version 13.2.1, the gpt/large_projects/giltabhq1 is going to need to be imported manually...
* To `docker run` in the background use `-d=true` or just `-d` command
  * In addition, to reattach to a detached container, use docker `attach` command

  ```shell
  docker ps -a
  docker attach --name <image_name>   # OR docker attach <container_id>
  ```
  * Type `Ctrl+p` then `Ctrl+q` to turn interactive mode to daemon mode
* To remove container when it exists add `--rm` command

## File system performance benchmarking

File system performance has a big impact on overall GitLab performance, especially for actions that read or write to Git repositories.

To install the `fio` package:

* On Ubuntu: `apt install fio`
* On yum-managed environments: `yum install fio`

Then run the following:

```shell
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --bs=4k --iodepth=64 --readwrite=randrw --rwmixread=75 --size=4G --filename=/path/to/git-data/testfile
```

## Best Practices

* When running locally i.e. against a test GitLab instance use `20s_*` options configs
* On a production i.e. customer instance use `60s_*` (full) options configs
  * Also for AWS it helps get past burstable (network) moments
* Use `*_20rps` options configs per 1k customers (e.g. `*_40rps` for 2k)
  * These reference options are optimized for 4x the actual user count i.e. they are very robust
* Additionally run [File system performance benchmarking](#file-system-performance-benchmarking)

## References

* [Environment configs](https://gitlab.com/gitlab-org/quality/performance/-/tree/master/k6/config/environments)
* [Options configs](https://gitlab.com/gitlab-org/quality/performance/-/tree/master/k6/config/options)
* [File system performance benchmarking](https://docs.gitlab.com/ee/administration/operations/filesystem_benchmarking.html)
