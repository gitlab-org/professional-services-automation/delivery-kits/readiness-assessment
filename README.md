> :warning: **This Delivery kit has been Archived and nested within [Implementation Delivery Kit](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/implementation-delivery-kits/implementation-delivery-kit)** :warning:
# Health Check

The Health check services are positioned to customers to help them feel comfortable that their deployment/configuration of GitLab self managed is ready for production workloads or to help prepare for a planned increase in workload. This project will help document the steps taken to deliver these services for a customer.

## Sales / Positioning Resources

The term Health Check is sometimes used interchangeably with readiness assessment. We currently have positioned and/or sold 3 different variations of the health check: 

  1. [Health Check SKU](https://drive.google.com/file/d/1OWZdw44MMaYLyrvxGo96vYuzz5wTXeaq/view)
    - [Proposal Deck Template](https://docs.google.com/presentation/d/1tumDRqygcTye8mnA0XYF-vT3OaujRgZABQ_lYd3Dhw4/edit#slide=id.g2c5958c4c8f_0_180)
  2. Advanced Health Check**
    - [Proposal Deck Template](https://docs.google.com/presentation/d/1euo9LsPamQvE2G4ca49t0eCtS7xBdcORsrxY3S15Ba0/edit#slide=id.g2c5958c4c8f_0_180)
  3. Transformative Health Check**
    - [Proposal Deck Template](https://docs.google.com/presentation/d/1cX-2inC3lojSk852bFS1ydlMWEvYvuO4Cg4_0DGpD2U/edit#slide=id.g2c5958c4c8f_0_180)

**Note: These 2 Health Checks were newly created out of customer demand. The Transformative Health Check has been sold and delivered, but the Advanced Health Check has not been. As of today, they don't exist on the PS catalog, but if they become repeat sellers, a formal data sheet and SOW will be created accordingly.

- [TEMPLATE Health Check Report](https://docs.google.com/document/d/1VQF26Ult7S0XVnH5zOquqwTa7sH8KCOOL8kL2sMx2pk/template/preview) for all health check offerings

## Health Check Service Matrix

<!-- | Service Name | Price (Fixed) | Duration | Transaction Details | Architecture & Performance Goals Disovery | Developer Experience KPI Definition | Run GPT or Prouction User Load Tests | Operation Runbooks for Backup, Restore, Upgrade| Runner & Application Architecture Design Advisory | Qualitative + Quantitative Developer Experience Analysis | Health Check Report |
| **Health Check SKU** | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| --- | $15,840 | Up to 2 weeks | Individual self-identified SKU | :white_check_mark: | :x: | :white_check_mark: | :x: | :x: | :x: | :white_check_mark: |
| **Advanced Health Check** | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| **Transformative Health Check** | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | -->
| Category | **Health Check SKU** | **Advanced Health Check** | **Transformative Health Check** |
|----|----|----|----|
| Price (Fixed) | $16,500 | $33,000 | $49,500 |
| Duration | Up to 2 weeks | 2-4 weeks | 3-9 weeks |
| Transaction Details | Individual SKU | 2 Consulting Block SKUs | 3 Consulting Block SKUs |
| Architecture & Performance Goals Discovery | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Developer Experience KPI Definition | :x: | :x: | :white_check_mark: |
| Run GPT or Production User Load Tests | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Operational runbooks for backup, restore and upgrade | :x: | :white_check_mark: | :x: |
| Runner & Application Architecture Design Advisory | :x: | :x: | :white_check_mark: |
| Qualitative/Quantitative Developer Experience Analysis | :x: | :x: | :white_check_mark: |
| Health Check Report with Recommendations | :white_check_mark: | :white_check_mark: | :white_check_mark: |

## Delivery options

- Delivering as [SKU](/health-check-sku.md)
- Delivering as [custom SOW](/health-check-custom.md)

_Edit this delivery kit by following our [contribution guidelines](/CONTRIBUTING.md)_
